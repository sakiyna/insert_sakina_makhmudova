-- Adding favourite film
INSERT INTO film (title, rental_rate, rental_duration, language_id)
VALUES ('Interstellar', 4.99, 14, 1);

--Adding actors
INSERT INTO actor (first_name, last_name)
VALUES ('Matthew', 'McConaughey'),
       ('Anne', 'Hathaway'),
       ('Jessica', 'Chastain');
      
INSERT INTO film_actor (film_id, actor_id)
SELECT film_id, actor_id
FROM film
CROSS JOIN actor
WHERE film.title = 'Interstellar'
AND actor.first_name IN ('Matthew', 'Anne', 'Jessica')
AND actor.last_name IN ('McConaughey', 'Hathaway', 'Chastain');

-- Adding to store
INSERT INTO inventory (film_id, store_id)
SELECT film_id, store_id
FROM film, store
WHERE film.title = 'Interstellar';



